# Scenes

Ease the use of remote sensing products provided by Dinamis/Theia.

Supported products:
- Spot 6/7
- Sentinel-2 Level 2 (THEIA)
- Sentinel-2 Level 3 (THEIA)

# Documentation

[Here](https://umr-tetis.gitlab.irstea.page/scenes)

# Install

The following libraries are required: OTB, GDAL (both with python bindings), and pyotb. The `libgnutls28-dev` package is required on ubuntu, to use the `scenes.download` module.
```commandline
pip install --upgrade pyotb
sudo apt update && sudo apt install -y libgnutls28-dev
pip install git+https://gitlab.irstea.fr/umr-tetis/scenes.git
```

# How to contribute?

Scenes follows the **gitflow** workflow.
All you have to do is:
1. Start from **develop** branch

```
git checkout develop
```

2. Create a new branch

```
git checkout -b 12-my_new_feature  # say somebody has opened an issue (#12) on gitlab
```

3. Push the new branch

```
git push origin 12-my_new_feature
```

4. Create a **merge request** from gitlab (**Left panel** > **Merge requests** > **New merge request**) or simply by copying the link displayed after the git push.
The merge request will trigger the CI (static analysis, tests, etc). 
To be merged, the new branch has to pass all tests.
Then, the new branch will be reviewed and merged.
