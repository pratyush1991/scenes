# Scenes

`Scenes` is a small python library aiming to provide a unified access to common remote sensing products.
Currently, supported sensors are:

- Spot 6
- Spot 7
- Sentinel-2 (Theia, Level 2)
- Sentinel-2 (Theia, Level 3)

## Generic features

`Scene` instances have generic methods and attributes.

- **Metadata**: metadata access with the `get_metadata()` method.
``` py
for key, value in sc.get_metadata():
  print(f"md[{key}]: {value}")
```
- **Acquisition date**: the `acquisition_date` member returns the acquisition date in the form of a `datetime.datetime` instance.
- **EPSG code**: the `epsg` member is an int for the EPSG of the scene.
- **Bounding box**: the `bbox_wgs84` member is a bounding box in WGS84 (EPSG 4326) coordinate reference system.

## Spot-6 and Spot-7

### Instantiation

Spot 6/7 scenes are instantiated from XS and PAN XML files of DIMAPS products.

``` py
from scenes.spot import Spot67Scene
sc = Spot67Scene(dimap_file_xs="/path/to/DIM_SPOT7_MS_..._1.XML",
                 dimap_file_pan="/path/to/DIM_SPOT7_P_..._1.XML")
```

### Image sources

Spot-6/7 scenes have 3 images sources.

| Source | `Scene` method | Description                                                        |
|--------|----------------|--------------------------------------------------------------------|
| XS     | `get_xs()`     | 4 channels (Red, Green, Blue, Near Infrared), ~6m physical spacing | 
| Pan    | `get_pan()`    | 1 channel (visible domain), ~1.5m physical spacing                 | 
| PXS    | `get_pxs(method=...)` | 4 channels (same as XS), ~1.5m physical spacing             |  

Each source can be computed in DN/TOA/TOC reflectance.

``` py
xs_raw = sc.get_xs()  # DN
xs_raw = sc.get_xs(reflectance="dn")  # DN
xs_toa = sc.get_xs(reflectance="toa")  # TOA
xs_toc = sc.get_xs(reflectance="toc")  # TOC
```

Each source can be masked with the cloud masks of the original product.
The no-data value can be chosen.

``` py
xs_toa_cld = xs_toa.cld_msk_drilled()  # (1)
```

1. To change the no-data inside the clouds mask: `masked_src = src.cld_msk_drilled(nodata=-999)`

### Examples

Computing NDVI from XS image in TOA reflectance:

``` py
xs = toa.get_xs(reflectance="toa")       # (1)
exp = "(im1b4-im1b1)/(im1b4+im1b1)"
ndvi = pyotb.bandmath(exp=exp, il=[xs])  # (2)
ndvi.write("ndvi.tif")
```

1. `xs` is a `scenes.spot.Spot67Source` instance
2. `ndvi` is a `pyotb.app` that inputs `xs`

The next example is a set of preprocessing operations on a Spot-6/7 XS image:

1. TOA reflectance
2. pansharpening
3. replacing the pixels under the clouds masks with "0"
4. clip the result over a reference raster

``` py
pxs = sc.get_pxs(reflectance="toa")        # (1)
drilled = pxs.cld_msk_drilled()            # (2)
ref_img = "/tmp/S2A_2020...._FRE_10m.tif"
subset = drilled.clip_over_img(ref_img)    # (3)
subset.write("subset.tif")
```

1. `pxs` is a `scenes.spot.Spot67Source` instance
2. `drilled` is a `scenes.spot.Spot67Source` instance
3. `subset` is a `scenes.spot.Spot67Source` instance

Note that we could have changed the no-data value in the cloud masking:

``` py
drilled = pxs.drilled(nodata=-999)
```

Superimpose an image over a reference image.
In the example below, `ref_img` is another `scenes.core.Source` instance.

``` py
toa = sc.get_pxs(reflectance="toa")
superimposed = toa.resample_over(ref_img)
superimposed.write("superimposed.tif")
```

## Sentinel-2 

Currently, Level-2 and Level-3 products from the [Theia land data center](https://www.theia-land.fr/en/product/sentinel-2-surface-reflectance/) are supported.

### Instantiation

Sentinel-2 scenes are instantiated from the product archive or folder.

``` py
from scenes.sentinel import Sentinel22AScene, Sentinel23AScene
sc_level2 = Sentinel22AScene("/path/to/SENTINEL2A_..._V1-8/.zip")
sc_level3 = Sentinel23AScene("/path/to/SENTINEL2X_...L3A_T31TEJ_D/.zip")
```

### Sources

Sentinel-2 images include 2 sources.

| Source    | `Scene` method    | Description                                               |
|-----------|-------------------|-----------------------------------------------------------|
| 10m bands | `get_10m_bands()` | 10m physical spacing spectral bands (4, 3, 2, 8)          | 
| 20m bands | `get_20m_bands()` | 20m physical spacing spectral bands (5, 6, 7, 8a, 11, 12) | 

Each source can be drilled with no-data values.

For Level 2 products, using the cloud mask:

``` py
src = sc_level2.get_10m_bands()
masked_src = src.cld_msk_drilled()  # (1)
```

1. To change the no-data inside the clouds mask: `masked_src = src.cld_msk_drilled(nodata=-999)`

For Level 3 products, using the quality mask:

``` py
src = sc_level3.get_10m_bands()
masked_src = src.flg_msk_drilled()  # (1)
```

1. To change the no-data inside the clouds mask: `masked_src = src.flg_msk_drilled(nodata=-999)`.
Also, the `keep_flags_values` can be used to select the pixels to keep from a list of values in the quality mask (land, water, snow, ...).

## Spatial and temporal indexation

Scenes includes a module to perform spatial and temporal indexation of `Scene` instances.

### Query in space

Perform a query in space (WGS84 bounding box) and time (optional) with an indexation structure.
``` py
from scenes import spot, indexation
scs = spot.get_spot67_scenes(root_dir)  # (1)
idx = indexation.Index(scenes)          # (2)
matches = idx.find(bbox_wgs84=bbox)     # (3)
for sc in matches:
  print(sc)
```

1. `scs` is a list of `scenes.core.Scene` instances
2. `idx` is a `scenes.indexation.Index` instance, namely a spatio-temporal index
3. `matches` is a list of `scenes.core.Scene` instances

### Query in space and time

To perform searches in time:
``` py
matches1 = idx.find(bbox_wgs84=bbox, "01/02/2019", "01-12-2020")  # (1)
matches2 = idx.find(bbox_wgs84=bbox, "01/02/2019")                # (2)
matches3 = idx.find(bbox_wgs84=bbox, date_max="01/02/2019")       # (3)
```

1. A few date formats are supported: "dd/mm/yyyy", "dd-mm-yyyy", and "yyyy-mm-dd"
2. Here we don't have upper bound
3. You can also specify only an upper bound, without lower bound (use the keywords `date_min` and `date_max`


## Architecture

### Scene class

The scene class handles all the metadata and the image sources.


``` mermaid
classDiagram

    Scene <|-- Spot67Scene
    Scene <|-- Sentinel2SceneBase
    Sentinel2SceneBase <|-- Sentinel22AScene
    Sentinel2SceneBase <|-- Sentinel23AScene

    Scene --*  Source: root_scene

    class Scene{
        +datetime acquisition_date
        +int epsg
        +bbox_wgs84
        +get_metadata()
        +__repr__()
    }

    class Spot67Scene{
        +float azimuth_angle
        +float azimuth_angle_across
        +float azimuth_angle_along
        +float incidence_angle
        +float sun_azimuth_angle
        +float sun_elev_angle
        +get_metadata()
        +Spot67Source get_xs()
        +Spot67Source get_pan()
        +Spot67Source get_pxs()

    }
    
    class Sentinel2SceneBase{
        +__init__(archive, tag)
        +get_file()
        +get_band()
        +get_metadata()
        +Sentinel2Source get_10m_bands()
        +Sentinel2Source get_20m_bands()

    }

    class Sentinel22AScene{
        +__init__(archive)
        +str clm_r1_msk_file
        +str clm_r2_msk_file
        +str edg_r1_msk_file
        +str edg_r2_msk_file
        +get_metadata()
    }
    
    class Sentinel23AScene{
        +__init__(archive)
        +str flg_r1_msk_file
        +str flg_r2_msk_file
        +get_metadata()  
    }

```

### Source class

The source stores the image pipeline that delivers the pixels.

``` mermaid
classDiagram

    Source <|-- Spot67Source
    Source <|-- Sentinel2Source
    Sentinel2Source <|-- Sentinel22ASource
    Sentinel2Source <|-- Sentinel23ASource
   
    class Source{
        +__init__(root_scene, out, parent=None)
        +Scene root_scene
        +Source parent
        +Source drilled(msk_vec_file, nodata=0)
        +Source cld_msk_drilled(nodata=0)
        +Source resample_over(ref_img, interpolator="nn", nodata=0)
        +Source clip_over_img(ref_img)
        +Source clip_over_vec(ref_vec)
        +Source new_source(*args)
    }
    
    class Spot67Source{
        +Spot67Source cld_msk_drilled(nodata=0)
    }
    
    class Sentinel2Source{
        +R1_SIZE
        +R2_SIZE
        +Sentinel2Source msk_drilled(msk_dict, exp, nodata=0)
    }
    
    class Sentinel22ASource{
        +Sentinel22ASource cld_msk_drilled(nodata=0)
    }
    
    class Sentinel23ASource{
        +Sentinel23ASource flg_msk_drilled(keep_flags_values=(3, 4), nodata=0)
    }

```

### Implement a new sensor from scratch

You can implement quickly a new sensor in `scenes`.

- One or multiple `MySensorSource1`, ..., `MySensorSourceM` classes, inheriting from `scenes.core.Source`, and implementing the image access for the new sensor.
- A new `MySensorScene` class, inheriting from `scenes.core.Scene`. This class must provide one or multiple methods to its sources.

``` mermaid
classDiagram

    Source <|-- NewSensorSource
    Scene <|-- NewSensorScene
    NewSensorScene --*  NewSensorSource: root_scene
   
```
