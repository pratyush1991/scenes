# -*- coding: utf-8 -*-
import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="scenes",
    version="1.1.0",
    author="Rémi Cresson",
    author_email="remi.cresson@inrae.fr",
    description="Library to ease the processing of local remote sensing products",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.irstea.fr/remi.cresson/scenes",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Topic :: Scientific/Engineering :: GIS",
        "Topic :: Scientific/Engineering :: Image Processing",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
    ],
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    install_requires=["rtree", "pyotb", "pycurl", "tqdm"],
    keywords="remote sensing, otb, orfeotoolbox, orfeo toolbox, pyotb",
    scripts=["apps/drs_spot67_import.py", "apps/s2_download.py", "apps/s2_import.py", "apps/search.py"]
)
#package_dir={"": "src"},

