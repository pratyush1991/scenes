# -*- coding: utf-8 -*-
import unittest
import filecmp
from abc import ABC
import pyotb


class ScenesTestBase(ABC, unittest.TestCase):
    """
    Base class for tests
    """
    def compare_images(self, image, reference, roi=None, mae_threshold=0.01):
        """
        Compare one image (typically: an output image) with a reference
        :param image: image to compare
        :param reference: baseline
        :param roi: Region of interest [startx, starty, sizex, sizey]
        :param mae_threshold: mean absolute error threshold
        :return: boolean
        """

        nbchannels_reconstruct = pyotb.Input(image).shape[-1]
        nbchannels_baseline = pyotb.Input(reference).shape[-1]

        self.assertTrue(nbchannels_reconstruct == nbchannels_baseline)

        for i in range(1, nbchannels_baseline + 1):
            dic = {'ref.in': reference,
                   'ref.channel': i,
                   'meas.in': image,
                   'meas.channel': i}
            if roi:
                dic.update({"roi.startx": roi[0],
                            "roi.starty": roi[1],
                            "roi.sizex": roi[2],
                            "roi.sizey": roi[3]})
            comp = pyotb.CompareImages(dic)
            self.assertTrue(comp.GetParameterFloat('mae') < mae_threshold)

    def compare_file(self, file, reference):
        """
        Compare two files

        Args:
            file: file to compare
            reference: baseline

        Return:
             a boolean
        """
        self.assertTrue(filecmp.cmp(file, reference))
