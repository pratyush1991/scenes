# -*- coding: utf-8 -*-
from scenes_test_base import ScenesTestBase
import pyotb
import tests_data


class Spot67ImageryTest(ScenesTestBase):

    def compare(self, inp, ref, reflectance):
        actual_ref = pyotb.OpticalCalibration(ref) if reflectance=="toa" else ref
        self.compare_images(inp, actual_ref)

    def xs_imagery(self, reflectance):
        xs = tests_data.SCENE1.get_xs(reflectance=reflectance)
        self.compare(xs, tests_data.DIMAP1_XS, reflectance)

    def pan_imagery(self, reflectance):
        pan = tests_data.SCENE1.get_pan(reflectance=reflectance)
        self.compare(pan, tests_data.DIMAP1_P, reflectance)

    def test_xs_dn(self):
        self.xs_imagery("dn")

    def test_pan_dn(self):
        self.pan_imagery("dn")

    def test_xs_toa(self):
        self.xs_imagery("toa")

    def test_pan_toa(self):
        self.pan_imagery("toa")

    def pxs_compare(self, pxs, reflectance):
        pxs_baseline = pyotb.BundleToPerfectSensor({"inxs": pyotb.OpticalCalibration(tests_data.DIMAP1_XS) if reflectance=="toa" else tests_data.DIMAP1_XS,
                                                    "inp": pyotb.OpticalCalibration(tests_data.DIMAP1_P) if reflectance=="toa" else tests_data.DIMAP1_P,
                                                    "method": "bayes"})
        # We test the central area only, because bayes method messes with nodata outside image
        self.compare_images(pxs, pxs_baseline, roi=[2500, 2500, 500, 500])

    def pxs_imagery(self, reflectance):
        pxs = tests_data.SCENE1.get_pxs(reflectance=reflectance)
        self.pxs_compare(pxs, reflectance)

    def pxs_imagery_cld_msk_drilled(self, reflectance):
        pxs = tests_data.SCENE1.get_pxs(reflectance=reflectance)
        pxs_drilled = pxs.cld_msk_drilled()
        self.pxs_compare(pxs_drilled, reflectance)

    def test_pxs_dn(self):
        self.pxs_imagery("dn")

    def test_pxs_toa(self):
        self.pxs_imagery("toa")

    def test_pxs_drilled_dn(self):
        self.pxs_imagery_cld_msk_drilled("dn")

    def test_pxs_drilled_toa(self):
        self.pxs_imagery_cld_msk_drilled("toa")


if __name__ == '__main__':
    unittest.main()
