# -*- coding: utf-8 -*-
from scenes_test_base import ScenesTestBase
import datetime
from scenes import Index, BoundingBox, dates

class DatesTest(ScenesTestBase):


    def test_input_dates_formats(self):
        datestr1 = "04/10/1986"
        datestr2 = "04-10-1986"
        datestr3 = "1986-10-04"
        dt = datetime.datetime(year=1986, month=10, day=4)

        assert dates.any2datetime(datestr1) == dt
        assert dates.any2datetime(datestr2) == dt
        assert dates.any2datetime(datestr3) == dt

        # Index test
        idx = Index([])
        dummy_bbox = BoundingBox(0, 1, 2, 3)
        _ = idx.find(dummy_bbox, date_min=datestr1)
        _ = idx.find(dummy_bbox, date_min=datestr2)
        _ = idx.find(dummy_bbox, date_min=datestr3)
        _ = idx.find(dummy_bbox, date_max=datestr1)
        _ = idx.find(dummy_bbox, date_max=datestr2)
        _ = idx.find(dummy_bbox, date_max=datestr3)


if __name__ == '__main__':
    unittest.main()
