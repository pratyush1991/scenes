# -*- coding: utf-8 -*-
import os
from scenes import spot


# TEST_DATA_DIR environment variable
TEST_DATA_DIR = os.environ["TEST_DATA_DIR"]

# Filenames
DIMAP1_XS = TEST_DATA_DIR + "/input/ROI_1_Bundle_Ortho_GSD2015/PROD_SPOT6_001/VOL_SPOT6_001_A/IMG_SPOT6_MS_001_A/" \
                            "DIM_SPOT6_MS_201503261014386_ORT_SPOT6_20170524_1422391k0ha487979cy_1.XML"
DIMAP1_P = TEST_DATA_DIR + "/input/ROI_1_Bundle_Ortho_GSD2015/PROD_SPOT6_001/VOL_SPOT6_001_A/IMG_SPOT6_P_001_A/" \
                           "DIM_SPOT6_P_201503261014386_ORT_SPOT6_20170524_1422391k0ha487979cy_1.XML"
ROI_MTP_2154 = TEST_DATA_DIR + "/input/roi_mtp_2154.gpkg"
ROI_MTP_4326 = TEST_DATA_DIR + "/input/roi_mtp_4326.gpkg"

# Instances
SCENE1 = spot.Spot67Scene(dimap_file_xs=DIMAP1_XS, dimap_file_pan=DIMAP1_P)