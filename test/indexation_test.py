# -*- coding: utf-8 -*-
from scenes_test_base import ScenesTestBase
from scenes import Index, vector, BoundingBox
import tests_data

class ImageryTest(ScenesTestBase):

    def test_scene1_indexation(self):
        index = Index(scenes_list=[tests_data.SCENE1])
        for find in (index.find, index.find_indices):
            self.assertTrue(find(BoundingBox(43.706, 43.708, 4.317, 4.420)))
            self.assertFalse(find(BoundingBox(43.000, 43.001, 3.000, 3.001)))
            self.assertTrue(find(vector.get_bbox_wgs84(tests_data.ROI_MTP_4326)))
            self.assertTrue(find(vector.get_bbox_wgs84(tests_data.ROI_MTP_2154)))
            self.assertTrue(find(tests_data.ROI_MTP_4326))
            self.assertTrue(find(tests_data.ROI_MTP_2154))

    def test_epsg(self):
        self.assertTrue(tests_data.SCENE1.epsg == 2154)


if __name__ == '__main__':
    unittest.main()
