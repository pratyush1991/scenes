#!/usr/bin/env python3
"""
This application enables to save a collection of Sentinel-2 image into a pickle object.

```
s2_import
  --root_dirs /path/to/S2_3A/T31TEJ /path/to/S2_3A/T31TEK
  --out_pickle s2_collection
```

"""
import sys
import argparse
from scenes import save_scenes
from scenes.sentinel import get_local_scenes

def main(args):
    # Arguments
    parser = argparse.ArgumentParser(description="Test",)
    parser.add_argument("--root_dirs", nargs='+', help="Root directories containing S2 archives (.zip)", required=True)
    parser.add_argument("--out_pickle", help="Output pickle file", required=True)
    parser.add_argument("--tile_name", help="(Optional) Tile name")
    params = parser.parse_args(args)

    # Search all Sentinel-2 scenes
    s2_scenes = [get_local_scenes(root_dir=root_dir, tile=params.tile_name) for root_dir in params.root_dirs]

    # Save scenes in a pickle file
    save_scenes(s2_scenes, params.out_pickle)


if __name__ == "__main__":

    main(sys.argv[1:])