"""
This module contains stuff for the spatio-temporal indexation of scenes.

The `scenes.indexation.Index` uses an R-Tree to perform the indexation of objects in the (x, y, t) domain.

---

Example: spatio-temporal indexation of multiple `scenes.core.Scene` instances.

## Build a spatio temporal index

``` py
import scenes
scenes_list = [...]  # a list of various `scenes.core.Scene` instances.
index = scenes.indexation.Index(scenes_list)  # build the index
```

## Search from a `BoundingBox`

The `find_indices()` method returns the indices of the indexed `scenes.core.Scene` instances matching
the query.
The `find()` method returns the indexed `scenes.core.Scene` instances matching the query.
``` py
bbox = BoundingBox(43.706, 43.708, 4.317, 4.420)
indices_results = index.find_indices(bbox)  # spatial query returning scenes indices
scenes_results = index.find(bbox)  # spatial query returning scenes instances
```

## Spatio-temporal search

A date min and/or a date max can be added in the query.
``` py
scenes_results = index.find(bbox, "01/01/2020" "01/01/2022")
```

## Search from a vector data file

The search can also be performed with a vector data file.
``` py
vec = "/path/to/vector.gpkg"
scenes_results = index.find(vec, "01/01/2020" "01/01/2022")
```

"""
from __future__ import annotations
import datetime
import rtree
from scenes.core import Scene
from scenes.dates import get_timestamp, any2datetime, MINDATE, MAXDATE
from scenes.vector import reproject_ogr_layer, get_bbox_wgs84, ogr_open
from scenes.spatial import poly_overlap, BoundingBox


def _bbox(bbox_wgs84: BoundingBox, date_min: datetime.datetime | str, date_max: datetime.datetime | str) -> tuple:
    """
    Return a bounding box in the domain (lat, lon, time)

    Args:
        bbox_wgs84: The bounding box in WGS84 (BoundingBox instance)
        date_min: date min (datetime.datetime or str)
        date_max: date max (datetime.datetime or str)

    Returns:
        item for rtree

    """
    return (bbox_wgs84.xmin, bbox_wgs84.ymin, get_timestamp(any2datetime(date_min)),
            bbox_wgs84.xmax, bbox_wgs84.ymax, get_timestamp(any2datetime(date_max)))


class Index:
    """
    Stores an indexation structure for a list of Scenes
    """

    def __init__(self, scenes_list: list[Scene]):
        """
        Args:
            scenes_list: list of scenes

        """
        self.scenes_list = scenes_list

        # Build RTrees (lat/lon/time)
        properties = rtree.index.Property()
        properties.dimension = 3
        self.index = rtree.index.Index(properties=properties)
        for scene_idx, scene in enumerate(scenes_list):
            dt = scene.acquisition_date
            dt_min = dt - datetime.timedelta(days=1)
            dt_max = dt + datetime.timedelta(days=1)
            new_bbox = _bbox(bbox_wgs84=scene.bbox_wgs84, date_min=dt_min, date_max=dt_max)
            self.index.insert(scene_idx, new_bbox)

    def find_indices(self, vector_or_bbox: str | BoundingBox, date_min: datetime.datetime | str = None,
                     date_max: datetime.datetime | str = None) -> list[int]:
        """
        Search the intersecting scenes, and return their indices

        The search is performed using the provided bounding box, or the provided input
        vector data file. The date_min and date_max are both optional.

        Args:
            vector_or_bbox: An input vector data file (str) or a bounding box in WGS84 (BoundingBox instance)
            date_min: date min (datetime.datetime or str) (Default value = None)
            date_max: date max (datetime.datetime or str) (Default value = None)

        Returns:
            list of indices

        """
        if not date_min:
            date_min = MINDATE
        if not date_max:
            date_max = MAXDATE

        # Use the bounding box from the vector data, or from the specified one
        actual_bbox_wgs84 = vector_or_bbox
        vector_file = isinstance(vector_or_bbox, str)
        if vector_file:
            actual_bbox_wgs84 = get_bbox_wgs84(vector_or_bbox)

        # Search in R-Tree
        bbox_search = _bbox(bbox_wgs84=actual_bbox_wgs84, date_min=date_min, date_max=date_max)
        results_indices = list(self.index.intersection(bbox_search))

        # Filter results (if vector_file)
        if vector_file:
            poly_ds = ogr_open(vector_file=vector_or_bbox)
            poly_layer_wgs84 = reproject_ogr_layer(poly_ds.GetLayer(), epsg=4326)
            filtered_indices = []
            for i in results_indices:
                bbox_wgs84_geom = self.scenes_list[i].bbox_wgs84.to_ogrgeom()
                if poly_overlap(poly_layer_wgs84, bbox_wgs84_geom) > 0:
                    filtered_indices.append(i)
            return filtered_indices
        return results_indices

    def find(self, vector_or_bbox: str | BoundingBox, date_min: datetime.datetime | str = None,
             date_max: datetime.datetime | str = None) -> list[Scene]:
        """
        Search the intersecting scenes, and return them

        Args:
            vector_or_bbox: An input vector data file (str) or a bounding box in WGS84 (BoundingBox instance)
            date_min: date min (datetime.datetime or str) (Default value = None)
            date_max: date max (datetime.datetime or str) (Default value = None)

        Returns:
            list of indices

        """
        indices = self.find_indices(vector_or_bbox=vector_or_bbox, date_min=date_min, date_max=date_max)
        return [self.scenes_list[i] for i in indices]
