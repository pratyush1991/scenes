"""
This module contains classes to work with Spot 6/7 products.
In particular, it specializes `scenes.core.Source` and `scenes.core.Scene`
for Spot 6/7 products.

---

# `Scene` based class

The `Spot67Scene` class carries metadata and image sources for Spot-6/7 sensors.

``` mermaid
classDiagram

    Scene <|-- Spot67Scene

    class Scene{
        +datetime acquisition_date
        +int epsg
        +bbox_wgs84
        +get_metadata()
        +__repr__()
    }

    class Spot67Scene{
        +float azimuth_angle
        +float azimuth_angle_across
        +float azimuth_angle_along
        +float incidence_angle
        +float sun_azimuth_angle
        +float sun_elev_angle
        +get_metadata()
        +Spot67Source get_xs()
        +Spot67Source get_pan()
        +Spot67Source get_pxs()
    }
```

## Instantiation

A `Spot67Scene` is instantiated from the .XML DIMAP files of PAN and XS:
``` py
import scenes
sc = scenes.spot.Spot67Scene(dimap_file_xs="DIM_SPOT6_MS..._1.XML",
                             dimap_file_pan="DIM_SPOT6_P..._1.XML")
```

## Metadata

The scene metadata can be accessed with the `get_metadata()` method, like any
`scenes.core.Scene` instance.
``` py
md_dict = sc.get_metadata()
for key, value in md_dict.items():
    print(f"{key}: {value}")
```

## Sources

The `Spot67Source` is the class for the different Spot-6/7 sources.

``` mermaid
classDiagram

    Source <|-- Spot67Source

        class Source{
        +__init__(root_scene, out, parent=None)
        +Scene root_scene
        +Source parent
        +Source drilled(msk_vec_file, nodata=0)
        +Source cld_msk_drilled(nodata=0)
        +Source resample_over(ref_img, interpolator="nn", nodata=0)
        +Source clip_over_img(ref_img)
        +Source clip_over_vec(ref_vec)
        +Source new_source(*args)
    }

    class Spot67Source{
        +Spot67Source cld_msk_drilled(nodata=0)
    }

```

### Different sources

The `Spot67Scene` delivers three `Spot67Source` instances:

- The multispectral image (xs)
- The Panchromatic image (pan)
- The pansharpenend image (pxs)
``` py
xs = sc.get_xs()
pan = sc.get_pan()
pxs = sc.get_pxs(method="bayes")
```

### Radiometry

Sources radiometry level can be selected when requesting the source.
Three level of radiometry are available for Spot-6/7 images:

- DN (Digital Number: raw sensor values)
- TOA (Top Of Atmosphere)
- TOC (Top Of Canopy)

``` py
p_dn = sc.get_pan()
xs_toa = sc.get_xs(reflectance="toa")
pxs_toc = sc.get_pxs(reflectance="toc")
```

The `Spot67Source` implements the `Spot67Source.cld_msk_drilled` method, that
enable to mask the cloud masks over the root source, with the specified
no-data value (default is 0).
The following example show how to derive a child source replacing the
pixels that are in the clouds with zero-valued pixels:
``` py
pxs_drilled = pxs.cld_msk_drilled()
```

The `Spot67Source` inherits from `scenes.core.Source`, hence implemented
sources transformations (e.g. `scenes.core.Source.masked()`, `scenes.core.Source.clip_over_img()`,
`scenes.core.Source.resample_over()`, `scenes.core.Source.reproject()`, etc.
``` py
clipped = pxs_drilled.clip_over_img(roi)
reprojected = clipped.reproject(epsg=4328)
```
Note that the resulting transformed `Spot67Source` is still an instance of
`Spot67Source` even after generic operations implemented in `scenes.core.Source`.

# Usage with pyotb

As `scenes.core.Source`, it also can be used like any `pyotb.core.otbObject`.
The following example show how to use an OTB application with a source at input.
``` py
rgb_nice = pyotb.DynamicConvert(reprojected)
rgb_nice.write("image.tif", pixel_type="uint8")
```

"""
from __future__ import annotations
import datetime
import xml.etree.ElementTree as ET
from tqdm.autonotebook import tqdm
import pyotb
from scenes import utils
from scenes.core import Source, Scene
from scenes.raster import get_epsg_extent_bbox
from scenes.spatial import extent_overlap


def find_all_dimaps(pth: str) -> list[str]:
    """
    Return the list of DIMAPS XML files that are inside all subdirectories of the root directory.

    Args:
        pth: root directory

    Returns:
        list of DIMAPS XML files

    """
    return utils.find_files_in_all_subdirs(pth=pth, pattern="DIM_*.XML")


def get_spot67_scenes(root_dir: str) -> list[Spot67Scene]:
    """
    Return the list of scenes that can be instantiated from a root directory containing
    a "PAN" and a "MS" subdirectories.

    Args:
        root_dir: directory containing "MS" and "PAN" subdirectories

    Returns:
        list of Spot67Scenes instances

    """
    # List files
    look_dir = root_dir + "/MS"
    print(f"Find files in {look_dir}")
    dimap_xs_files = find_all_dimaps(look_dir)
    print(f"Found {len(dimap_xs_files)} DIMAP files in MS folder", flush=True)

    # Create scenes list
    scenes = []
    errors = {}
    for dimap_file_xs in tqdm(dimap_xs_files):
        try:
            # Find pairs of XS/PAN DIMAPS
            pan_path = dimap_file_xs[:dimap_file_xs.find("/PROD_SPOT")]
            pan_path = pan_path.replace("/MS/", "/PAN/")
            pan_path = pan_path.replace("_MS_", "_PAN_")
            dimap_pan_files = find_all_dimaps(pan_path)
            nb_files = len(dimap_pan_files)
            if nb_files != 1:
                raise ValueError(f"{nb_files} DIMAPS candidates found in {pan_path} ")
            dimap_file_pan = dimap_pan_files[0]
            # Instantiate a new scene object
            new_scene = Spot67Scene(dimap_file_pan=dimap_file_pan, dimap_file_xs=dimap_file_xs)
            scenes.append(new_scene)
        except Exception as error:
            if dimap_file_xs not in errors:
                errors[dimap_file_xs] = []
            errors[dimap_file_xs].append(error)
            raise

    print(f"{len(scenes)} scenes imported")

    if errors:
        print(f"{len(errors)} scenes could not have been imported.")
        for dimap_file_xs, error_list in errors.items():
            print(f"Errors for {dimap_file_xs}:")
            for error in error_list:
                print(f"\t{error}")

    return scenes


class Spot67Source(Source):
    """
    Spot 6/7 source class
    """

    def cld_msk_drilled(self, nodata: float | int = 0) -> Spot67Source:
        """
        Return the source drilled from the cloud mask

        Args:
            nodata: nodata value inside holes (Default value = 0)

        Returns:
            drilled source

        """
        return self.drilled(self.root_scene.cld_msk_file, nodata=nodata)


class Spot67Scene(Scene):
    """
    Spot 6/7 root_scene class.

    The Spot67Scene class carries all metadata and images sources from the scene.
    A Spot67Scene object can be instantiated from the XS and PAN DIMAPS (.XML) file.

    """
    PXS_OVERLAP_THRESH = 0.995

    def __init__(self, dimap_file_xs: str, dimap_file_pan: str):
        """
        Args:
            dimap_file_xs: XML DIMAP file for the XS product
            dimap_file_pan: XML DIMAP file for the PAN product

        """

        # DIMAP files
        def _check_is_dimap(dimap_file):
            if not dimap_file.lower().endswith(".xml"):
                raise FileNotFoundError(f"An input DIMAP file is needed. This file is not a .xml: {dimap_file}")
            return dimap_file

        self.dimap_file_xs = _check_is_dimap(dimap_file_xs)
        self.dimap_file_pan = _check_is_dimap(dimap_file_pan)

        # Cloud masks
        def _get_mask(dimap_file, pattern):
            """
            Retrieve GML file.

            Args:
                dimap_file: DIMAP file
                pattern: vector data file pattern

            Returns:
                path of the file

            Raises:
                FileNotFoundError: when multiple files are found.

            """
            cld_path = utils.get_parent_directory(dimap_file) + "/MASKS"
            plist = utils.find_file_in_dir(cld_path, pattern=pattern)
            if len(plist) != 1:
                raise FileNotFoundError(
                    f"ERROR: unable to find a unique file in {cld_path} with pattern {pattern}")
            return plist[0]

        # Get metadata
        tree = ET.parse(dimap_file_xs)
        root = tree.getroot()

        # Acquisition angles
        c_nodes = root.find("Geometric_Data/Use_Area/Located_Geometric_Values/Acquisition_Angles")
        for node in c_nodes:
            if node.tag == "AZIMUTH_ANGLE":
                self.azimuth_angle = float(node.text)
            elif node.tag == "VIEWING_ANGLE_ACROSS_TRACK":
                self.viewing_angle_across = float(node.text)
            elif node.tag == "VIEWING_ANGLE_ALONG_TRACK":
                self.viewing_angle_along = float(node.text)
            elif node.tag == "VIEWING_ANGLE":
                self.viewing_angle = float(node.text)
            elif node.tag == "INCIDENCE_ANGLE":
                self.incidence_angle = float(node.text)

        # Acquisition date
        c_nodes = root.find("Geometric_Data/Use_Area/Located_Geometric_Values")
        for node in c_nodes:
            if node.tag == "TIME":
                acquisition_date = datetime.datetime.strptime(node.text[0:10], "%Y-%m-%d")
                break

        # Sun angles
        c_nodes = root.find("Geometric_Data/Use_Area/Located_Geometric_Values/Solar_Incidences")
        for node in c_nodes:
            if node.tag == "SUN_AZIMUTH":
                self.sun_azimuth = float(node.text)
            elif node.tag == "SUN_ELEVATION":
                self.sun_elevation = float(node.text)

        # Get EPSG and bounding box
        epsg_xs, extent_wgs84_xs, self.bbox_wgs84_xs = get_epsg_extent_bbox(dimap_file_xs)
        epsg_pan, extent_wgs84_pan, self.bbox_wgs84_pan = get_epsg_extent_bbox(dimap_file_pan)

        # Check that EPSG for PAN and XS are the same
        if epsg_pan != epsg_xs:
            raise ValueError(f"EPSG of XS and PAN sources are different:  XS EPSG is {epsg_xs}, PAN EPSG is {epsg_pan}")

        # Here we compute bounding boxes overlap, to choose the most appropriated
        # CLD and ROI masks for the scene. Indeed, sometimes products are not
        # generated as they should (i.e. bundle) and XS and PAN have different extents
        # so CLD and ROI masks are not the same for XS and PAN.
        # We keep the ROI+CLD masks of the PAN or XS lying completely inside
        # the other one.
        self.xs_overlap = extent_overlap(extent_wgs84_xs, extent_wgs84_pan)
        self.pan_overlap = extent_overlap(extent_wgs84_pan, extent_wgs84_xs)

        # Get ROI+CLD filenames in XS and PAN products
        self.cld_msk_file_xs = _get_mask(dimap_file_xs, "CLD*.GML")
        self.cld_msk_file_pan = _get_mask(dimap_file_pan, "CLD*.GML")
        self.roi_msk_file_xs = _get_mask(dimap_file_xs, "ROI*.GML")
        self.roi_msk_file_pan = _get_mask(dimap_file_pan, "ROI*.GML")

        # Choice based on the pxs overlap
        bbox_wgs84 = self.bbox_wgs84_xs
        self.cld_msk_file = self.cld_msk_file_xs
        self.roi_msk_file = self.roi_msk_file_xs
        self.pxs_overlap = self.xs_overlap
        if self.pan_overlap > self.xs_overlap:
            bbox_wgs84 = self.bbox_wgs84_pan
            self.cld_msk_file = self.cld_msk_file_pan
            self.roi_msk_file = self.roi_msk_file_pan
            self.pxs_overlap = self.pan_overlap

        # Throw some warning or error, depending on the pxs overlap value
        msg = f"Bounding boxes of XS and PAN sources have {100 * self.pxs_overlap:.2f}% overlap. " \
              + f"\n\tXS bbox_wgs84: {self.bbox_wgs84_xs} \n\tPAN bbox_wgs84: {self.bbox_wgs84_pan}"
        if self.pxs_overlap == 0:
            raise ValueError(msg)
        if self.has_partial_pxs_overlap():
            raise Warning(msg)

        # Call parent constructor
        super().__init__(acquisition_date=acquisition_date, bbox_wgs84=bbox_wgs84, epsg=epsg_xs)

    def has_partial_pxs_overlap(self) -> bool:
        """
        Returns:
            True if at least PAN or XS lies completely inside the other one. False else.

        """
        return self.pxs_overlap < self.PXS_OVERLAP_THRESH

    def reflectance(self, inp: str | pyotb.core.otbObject, reflectance: str = "dn", clamp: bool = False,
                    factor: float = None):
        """
        This function is used internally by `get_xs()`, `get_pxs()`, and `get_pan()` to compute the
        reflectance (or not!) of the optical image.

        Args:
            inp: input
            reflectance: optional level of reflectance (can be "dn", "toa", "toc")
            clamp: normalize reflectance values
            factor: factor to scale pixel values (e.g. 10000)

        Returns:
            calibrated, or original image source

        """
        reflectance = reflectance.lower()
        assert reflectance in ("dn", "toa", "toc"), "reflectance can be 'dn', 'toa' or 'toc'"

        # Base
        out = inp

        # Radiometry correction
        if reflectance in ("toa", "toc"):
            out = pyotb.OpticalCalibration({"in": inp, "level": reflectance, "clamp": clamp, "milli": False})

        if factor:
            out = out * factor

        return Spot67Source(self, out)

    def get_xs(self, **kwargs) -> Spot67Source:
        """
        Returns the MS source

        Args:
            kwargs: same as the `reflectance` method

        Returns:
            A `Spot67Source` instance for the MS image

        """
        return self.reflectance(self.dimap_file_xs, **kwargs)

    def get_pan(self, **kwargs) -> Spot67Source:
        """
        Returns PAN source

        Args:
            kwargs: same as the `reflectance` method

        Returns:
            A `Spot67Source` instance for the PAN image

        """
        return self.reflectance(self.dimap_file_pan, **kwargs)

    def get_pxs(self, method: str = "bayes", **kwargs) -> Spot67Source:
        """
        Returns the pansharpened source

        Args:
            method: one of rcs, lmvm, bayes (Default value = "bayes")
            kwargs: same as the `reflectance` method

        Returns:
            A `Spot67Source` instance for the pansharpened image

        """
        xs = self.get_xs(**kwargs)
        pan = self.get_pan(**kwargs)
        # new source with pansharpening
        pansharp = Spot67Source(self, pyotb.BundleToPerfectSensor({"inp": pan, "inxs": xs, "method": method}))
        # new source masked outside the original ROI
        return Spot67Source(self, pansharp.drilled(msk_vec_file=self.roi_msk_file, inside=False))

    def get_metadata(self) -> dict[str, any]:
        """
        Get metadata.

        Returns:
            metadata

        """
        metadata = super().get_metadata()
        metadata.update({
            "DIMAP XS": self.dimap_file_xs,
            "DIMAP PAN": self.dimap_file_pan,
            "Cloud mask": self.cld_msk_file,
            "Cloud mask XS": self.cld_msk_file_xs,
            "Cloud mask PAN": self.cld_msk_file_pan,
            "ROI mask": self.roi_msk_file,
            "ROI mask XS": self.roi_msk_file_xs,
            "ROI mask PAN": self.roi_msk_file_pan,
            "Azimuth angle": self.azimuth_angle,
            "Viewing angle across track": self.viewing_angle_across,
            "Viewing angle along track": self.viewing_angle_along,
            "Viewing angle": self.viewing_angle,
            "Incidence angle": self.incidence_angle,
            "Sun elevation": self.sun_elevation,
            "Sun azimuth": self.sun_azimuth,
            "Bounding box XS (WGS84)": self.bbox_wgs84_xs,
            "Bounding box PAN (WGS84)": self.bbox_wgs84_pan
        })
        return metadata
