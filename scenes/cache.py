"""
This module provides mechanisms to enable pyotb raster caching on the local filesystem.
"""
from __future__ import annotations
import json
import hashlib
import tempfile
import os
import pyotb


class Cache(pyotb.Input):
    """
    Enable to manage a given pipeline output, depending on if it's already in the cache.
    """

    def __init__(self, pyotb_output, temporary_directory: str = None, output_parameter_key: str = None,
                 extension: str = None, pixel_type: str = None):
        """
        Initialize the cache.

        Args:
            pyotb_output: a pyotb.Output instance.
            temporary_directory: a temporary directory for the cached files. Default is system temp directory.
            output_parameter_key: output parameter key (default is first key)
            extension: file extension (default: .tif)
            pixel_type: pixel type
        """
        # Get app
        pyotb_app = pyotb_output.pyotb_app

        # Get summary
        summary = pyotb_app.summarize()  # need pyotb >= 1.5.1

        # Summary --> md5sum
        desc = json.dumps(summary)
        md5sum = hashlib.md5(desc.encode('utf-8')).hexdigest()

        # App name
        app_name = summary["name"]

        # Cache filename
        output_parameters_key = pyotb_app.output_param if not output_parameter_key else output_parameter_key
        extension = extension if extension else ".tif?&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES"
        if not extension.startswith("."):
            extension = f".{extension}"
        pixel_type = pixel_type if pixel_type else "float"
        tmpdir = temporary_directory if temporary_directory else tempfile.gettempdir()
        prefix = os.path.join(tmpdir, f"{app_name}_{output_parameters_key}_{md5sum}")
        cache_file = f"{prefix}{extension}"
        json_file = f"{prefix}.json"

        # Check which cache files already exist
        if not os.path.exists(json_file):
            # pyotb write
            pyotb_output.write(cache_file, pixel_type=pixel_type)
            # json
            with open(json_file, 'w', encoding='utf-8') as f:
                json.dump(summary, f, ensure_ascii=False, indent=4)

        super().__init__(filepath=cache_file.split("?")[0])
