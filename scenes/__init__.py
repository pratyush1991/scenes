# -*- coding: utf-8 -*-
"""
This module helps to process local remote sensing products
"""
__version__ = "1.1.0"

from os.path import dirname, basename, isfile, join
import glob
import importlib
from .core import load_scenes, save_scenes  # noqa: 401
from .indexation import Index  # noqa: 401
from .download import TheiaDownloader  # noqa: 401
from .spatial import BoundingBox  # noqa: 401
modules = glob.glob(join(dirname(__file__), "*.py"))
for f in modules:
    if isfile(f) and f.endswith('.py') and not f.endswith('__init__.py'):
        importlib.import_module(f".{basename(f)[:-3]}", __name__)
