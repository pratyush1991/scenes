"""
This module contains a set of generic purpose helpers
"""
from __future__ import annotations
import os
import glob
import pathlib
import zipfile
import re
import fnmatch


def find_files_in_all_subdirs(pth: str, pattern: str, case_sensitive: bool = True) -> list[str]:
    """
    Returns the list of files matching the pattern in all subdirectories of pth

    Args:
        pth: path
        pattern: pattern
        case_sensitive: boolean (Default value = True)

    Returns:
        list of str

    """
    result = []
    reg_expr = re.compile(fnmatch.translate(pattern), 0 if case_sensitive else re.IGNORECASE)
    for root, _, files in os.walk(pth, topdown=True):
        result += [os.path.join(root, j) for j in files if re.match(reg_expr, j)]
    return result


def find_file_in_dir(pth: str, pattern: str) -> list[str]:
    """
    Returns the list of files matching the pattern in the input directory

    Args:
        pth: path
        pattern: pattern

    Returns:
        list of str

    """
    return glob.glob(os.path.join(pth, pattern))


def get_parent_directory(pth: str) -> str:
    """
    Return the parent directory of the input directory or file

    Args:
        pth: input directory or file

    Returns:
        parent directory

    """
    path = pathlib.Path(pth)
    if not path:
        raise FileNotFoundError(f"Cannot define path: {path}")
    return str(path.parent)


def list_files_in_zip(filename: str, endswith: str = None) -> list[str]:
    """
    List files in zip archive

    Args:
        filename: path of the zip
        endswith: optional, end of filename to be matched (Default value = None)

    Returns:
        list of filepaths

    """
    with zipfile.ZipFile(filename) as zip_file:
        filelist = zip_file.namelist()
    if endswith:
        filelist = [f for f in filelist if f.endswith(endswith)]

    return filelist


def to_vsizip(zipfn: str, relpth: str) -> str:
    """
    Create path from zip file

    Args:
        zipfn: zip archive
        relpth: relative path (inside archive)

    Returns:
        vsizip path

    """
    return f"/vsizip/{zipfn}/{relpth}"


def basename(pth: str) -> str:
    """
    Returns the basename. Works with files and paths

    Args:
        pth: path

    Returns:
        basename of the path

    """
    return str(pathlib.Path(pth).name)
