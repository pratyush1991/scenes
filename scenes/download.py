# -*- coding: utf-8 -*-
"""
This module handles the download of Sentinel-2 images from Theia (L2A or L3A).

The `scenes.download.TheiaDownloader` uses Theia credentials.
Those must be stored in a file looking like this:

```
serveur = https://theia.cnes.fr/atdistrib
resto = resto2
token_type = text
login_theia = remi.cresson@irstea.fr
password_theia = thisisnotmyrealpassword
```

To instantiate the `scenes.download.TheiaDownloader`:
``` py
import scenes
cfg_file = "config.txt"  # Theia config file
theia = scenes.download.TheiaDownloader(cfg_file)
```

## Bounding box + temporal range

The following example shows how to use the `scenes.download.TheiaDownloader` to search
or download all Sentinel-2 images in a bounding box within a temporal range.

### Search

When the `download_dir` is not set, the download is not performed, and only the search results
are returned from the function.
``` py
bbox = scenes.spatial.BoundingBox(43.706, 43.708, 4.317, 4.420)
trange = ("01/01/2020", "O1/O1/2022")
results = theia.download_in_range(bbox, trange, level="LEVEL2A")
print(results)
```

### Download

To download the files, the `download_dir` must be specified:
``` py
theia.download_in_range(bbox, trange, "/tmp/download/", "LEVEL2A")
```
When files already exist, the md5sum is computed and compared with the one in the catalog,
in order to determine if it has to be downloaded again. If the file is already downloaded and
is complete according to the md5sum, its download is skipped.

## Bounding box + single date

In the same manner, the downloader can search or download the closest images from a specific date.
The returned dict from the function is updated with a "delta" key storing the value of the number of
days from the specific date.
``` py
# For searching only
results = theia.download_in_range(bbox, trange, "LEVEL2A")

# For downloading
theia.download_in_range(bbox, trange, "/tmp/download/", "LEVEL2A")
```

"""
from __future__ import annotations
import datetime
import hashlib
import io
import json
import os
from urllib.parse import urlencode

import pycurl
from tqdm.autonotebook import tqdm

from scenes import dates


def compute_md5(fname: str) -> str:
    """
    Compute md5sum of a file

    Args:
        fname: file name

    """
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def is_file_complete(filename: str, md5sum: str) -> bool:
    """
    Tell if a file is complete

    Args:
        filename: path of the file
        md5sum: reference md5

    """
    # Does the file exist?
    if not os.path.isfile(filename):
        return False

    # Does the file completed?
    return md5sum == compute_md5(filename)


def curl_url(url, postdata: dict[str, str], verbose: bool = False, out_file: str = None,
             header: list[str] = None) -> str:
    """
    Use PyCurl to make some requests

    Args:
        url: url
        postdata: POST data
        verbose: boolean (Default value = False)
        out_file: output file (Default value = None)
        header: header. If None is kept, ['Accept:application/json'] is used (Default value = None)

    Returns:
        decoded contents

    """
    if not header:
        header = ['Accept:application/json']

    c = pycurl.Curl()
    c.setopt(pycurl.URL, url)
    c.setopt(pycurl.HTTPHEADER, header)
    c.setopt(pycurl.SSL_VERIFYPEER, False)
    c.setopt(pycurl.SSL_VERIFYHOST, False)
    if postdata is not None:
        c.setopt(pycurl.POST, 1)
        postfields = urlencode(postdata)
        c.setopt(pycurl.POSTFIELDS, postfields)
    storage = io.BytesIO()
    if verbose:
        c.setopt(pycurl.VERBOSE, 1)
    if out_file is not None:
        with open(out_file, "wb") as fp:
            progress_bar = None
            last_download_d = 0
            print("Downloading", flush=True)

            def _status(download_t, download_d, *_):
                """Callback function for c.XFERINFOFUNCTION
                https://stackoverflow.com/questions/19724222/pycurl-attachments-and-progress-functions

                Args:
                    download_t: total
                    download_d: already downloaded
                    *_: any additional param (won't be used)

                """
                if download_d > 0:
                    nonlocal progress_bar, last_download_d
                    if not progress_bar:
                        progress_bar = tqdm(total=download_t, unit='iB', unit_scale=True)
                    progress_bar.update(download_d - last_download_d)
                    last_download_d = download_d

            c.setopt(c.NOPROGRESS, False)
            c.setopt(c.XFERINFOFUNCTION, _status)
            c.setopt(pycurl.WRITEDATA, fp)
            c.perform()
    else:
        c.setopt(pycurl.WRITEFUNCTION, storage.write)
        c.perform()
    c.close()
    content = storage.getvalue()
    return content.decode(encoding="utf-8", errors="strict")


class TheiaDownloader:
    """The TheiaDownloader class enables to download L2A and L3A Theia products"""
    def __init__(self, config_file: str, max_records: int = 500):
        """
        Args:
            config_file: Theia configuration file
                The file contents should look like this:

                ```
                serveur = https://theia.cnes.fr/atdistrib
                resto = resto2
                token_type = text
                login_theia = remi.cresson@irstea.fr
                password_theia = thisisnotmyrealpassword
                ```

            max_records: Maximum number of records

        """
        # Read the Theia config file
        self.config = {}
        with open(config_file, 'r', encoding="utf8") as r:
            for line in r.readlines():
                splits = line.split('=', 1)
                if len(splits) == 2:
                    self.config[splits[0].strip()] = splits[1].strip()

        # Check keys
        checking_keys = ["serveur", "resto", "login_theia", "password_theia", "token_type"]

        # Proxy
        if "proxy" in self.config:
            checking_keys.extend(["login_proxy", "password_proxy"])

        # Check keys
        for key_name in checking_keys:
            if key_name not in self.config:
                raise ValueError(f"error with config file, missing key : {key_name}")

        # Maximum number of records
        self.max_records = max_records

    @staticmethod
    def _bbox2str(bbox):
        """Return a str containing the bounding box

        Args:
            bbox: the bounding box (BoundingBox instance)

        Returns:
            a string

        """
        return f'{bbox.ymin},{bbox.xmin},{bbox.ymax},{bbox.xmax}'

    def _get_token(self):
        """Get the THEIA token"""
        postdata_token = {"ident": self.config["login_theia"], "pass": self.config["password_theia"]}
        url = f"{self.config['serveur']}/services/authenticate/"
        token = curl_url(url, postdata_token)
        if not token:
            print("Empty token. Please check your credentials in config file.")
        return token

    def _query(self, dict_query):
        """
        Search products

        Return a dict with the following structure
            TILE_NAME
               +---- DATE
                +------ id
                +------ url
                +------ checksum
                +------ product_name

        Args:
            dict_query: query

        Returns:
            tile dictionary

        """
        url = f"{self.config['serveur']}/{self.config['resto']}/api/collections/SENTINEL2/" \
              f"search.json?{urlencode(dict_query)}"
        print("Ask Theia catalog...")
        search = json.loads(curl_url(url, None))

        tiles_dict = {}
        for record in search["features"]:
            tile_name = record["properties"]["location"]  # T31TCJ
            acq_date = record["properties"]["completionDate"][0:10]  # YYYY-MM-DD
            new_acquisition = {
                "id": record["id"],
                "product_name": record["properties"]["productIdentifier"],  # SENTINEL2X_20191215-000000-000_L3A_T31...
                "checksum": record["properties"]["services"]["download"]["checksum"],
                "url": record["properties"]["services"]["download"]["url"]  # https://theia.cnes.fr/atdistr.../download
            }
            if tile_name not in tiles_dict:
                tiles_dict[tile_name] = {acq_date: new_acquisition}
            tiles_dict[tile_name].update({acq_date: new_acquisition})

        return tiles_dict

    def _download_tiles_and_dates(self, tiles_dict, download_dir):
        """
        Download a product.

        Updates the "tiles_dict" with the filename of the downloaded files

        Args:
            tiles_dict: tiles dictionary. Must have the following structure:
                       TILE_NAME
                         +---- DATE
                         +------ id
                         +------ url
                         +------ checksum
                         +------ product_name
          download_dir:

        Returns:
            tiles_dict updated with local_file:
                         +------ local_file

        """
        print("Get token...")
        token = self._get_token()
        print(f"OK ({token})")
        for tile_name, tile in tiles_dict.items():
            print(f"Fetching products for tile {tile_name}...")
            for acq_date, description in tile.items():
                url = f"{description['url']}/?issuerId=theia"
                header = [f'Authorization: Bearer {token}', 'Content-Type: application/json']
                filename = f"{os.path.join(download_dir, description['product_name'])}.zip"

                # Check if the destination file exist and is correct
                if not is_file_complete(filename, description["checksum"]):
                    print(f"Downloading {acq_date}")
                    curl_url(url, postdata=None, out_file=filename, header=header)
                else:
                    print(f"{acq_date} already downloaded. Skipping.")
                description["local_file"] = filename

        return tiles_dict

    def download_in_range(self, bbox_wgs84: list[float], dates_range: tuple(datetime.datetime, datetime.datetime),
                          download_dir: str = None, level: str = "LEVEL3A") -> dict[str, dict[str, str]]:
        """
        Download all images within spatial and temporal ranges

        Args:
            bbox_wgs84: bounding box (WGS84)
            dates_range: a tuple of datetime.datetime or str instances (start_date, end_date)
            download_dir: download directory (Default value = None)
            level: LEVEL2A, LEVEL3A, ... (Default value = "LEVEL3A")

        Returns:
            search results / downloaded files (dict)

        """
        start_date, end_date = dates_range

        dict_query = {
            "box": self._bbox2str(bbox_wgs84),  # lonmin, latmin, lonmax, latmax
            "startDate": dates.any2datetime(start_date).strftime("%Y-%m-%d"),
            "completionDate": dates.any2datetime(end_date).strftime("%Y-%m-%d"),
            "maxRecords": self.max_records,
            "processingLevel": level
        }

        # Search products
        search_results = self._query(dict_query)

        # Download products
        if download_dir:
            return self._download_tiles_and_dates(search_results, download_dir)

        return search_results

    def download_closest(self, bbox_wgs84: list[float], acq_date: datetime.datetime, download_dir: str = None,
                         level: str = "LEVEL3A") -> dict[str, dict[str, str]]:
        """
        Query Theia catalog, download_closest the files

        Args:
            bbox_wgs84: bounding box (WGS84)
            acq_date: acquisition date to look around (datetime.datetime or str)
            download_dir: download directory (Default value = None)
            level: LEVEL2A, LEVEL3A, ... (Default value = "LEVEL3A")

        Returns:
            search results / downloaded files (dict)

        """

        # Important parameters
        ndays_seek = datetime.timedelta(days=17)  # temporal range to check for monthly synthesis

        # Query products
        actual_date = dates.any2datetime(acq_date)
        dict_query = {'box': self._bbox2str(bbox_wgs84)}  # lonmin, latmin, lonmax, latmax
        start_date = actual_date - ndays_seek
        end_date = actual_date + ndays_seek

        dict_query['startDate'] = start_date.strftime("%Y-%m-%d")
        dict_query['completionDate'] = end_date.strftime("%Y-%m-%d")
        dict_query['maxRecords'] = self.max_records
        dict_query['processingLevel'] = level

        # Search products
        search_results = self._query(dict_query)

        # Sort by closest date (add the "Delta" key/value)
        for tile_name, tile in search_results.items():
            print(tile_name)
            for description_date in tile:
                print("\t" + description_date)
                dt = datetime.datetime.strptime(description_date, "%Y-%m-%d")
                delta = actual_date - dt
                delta = delta.days
                search_results[tile_name][description_date]["delta"] = delta  # pylint: disable=R1733

        # Rank delta
        selected_tile = {}
        for tile_name, x in search_results.items():
            n_dates = 0
            sorted_x = sorted(x.items(), key=lambda kv: abs(kv[1]["delta"]))
            selected_tile[tile_name] = {}
            for i in sorted_x:
                description_date = i[0]
                entry = i[1]
                selected_tile[tile_name][description_date] = entry
                n_dates += 1
                if n_dates == 1:
                    break

        # Print summary
        print("Best tiles/dates:")
        for tile_name, tile in selected_tile.items():
            print(f"Tile {tile_name}")
            print("\tDate (delta)")
            for description_date, description in tile.items():
                print(f"\t{description_date} ({description['delta']})")

        # Download products
        if download_dir:
            return self._download_tiles_and_dates(selected_tile, download_dir)

        return selected_tile
