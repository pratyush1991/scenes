"""

This module provides tools to easily interact with deep learning models.

OTBTF is needed to use this module.

---

## Super-resolution

The SR4RS model can be applied over any `scenes.core.Source` instance.
We recall that this model is intended to be used over Sentinel-2 optical images.
For example, here is how we perform the super-resolution of a Theia S2 product.
``` py
import scenes
archive = "SENTINEL2B_..._T31TEJ_D_V1-8.zip"
s2_scene = scenes.sentinel.Sentinel22AScene(archive)
s2_image = s2_scene.get_10m_bands()
sr = scenes.deepnets.sr4rs(s2_image)  # pyotb.core.otbObject
sr.write("sr.tif")
```
"""
from __future__ import annotations
import os
import zipfile
import pyotb
from scenes import download

SR4RS_MODEL_URL = "https://nextcloud.inrae.fr/s/boabW9yCjdpLPGX/download/" \
                  "sr4rs_sentinel2_bands4328_france2020_savedmodel.zip"


def inference(dic: dict[str, any]):
    """
    Generic function to perform deep nets inference.

    When OTBTF is not found, a warning message is printed.
    Args:
        dic: otb parameters dict

    Returns:
        pyotb App instance. When OTBTF is not found, None is returned.

    """
    output = None
    try:
        output = pyotb.TensorflowModelServe(dic)
    except AttributeError:
        print("OTBTF module has not been found in the system! It is mandatory to use deepnets. ")
    return output


def sr4rs(input_image: str | pyotb.core.otbObject, model_url: str = SR4RS_MODEL_URL,
          tmp_dir: str = "/tmp") -> pyotb.core.App:
    """
    Applies the SR4RS model for super-resolution

    See https://github.com/remicres/sr4rs for implementation details.

    Args:
        input_image: pyotb Input
        model_url: SR4RS pre-trained model URL. Must point to a online .zip file.
        tmp_dir: directory for temporary files.

    Returns:
        pyotb Output

    """
    efield = 512
    gen_fcn = 64
    ratio = 0.25
    rfield = int((efield + 2 * gen_fcn) * ratio)

    # download the model if not already here
    tmp_zip = os.path.join(tmp_dir, os.path.basename(model_url))
    tmp_unzipped = os.path.splitext(tmp_zip)[0]
    if not os.path.exists(tmp_unzipped):
        download.curl_url(model_url, postdata=None, out_file=tmp_zip)
        with zipfile.ZipFile(tmp_zip, 'r') as zip_ref:
            print("Unzipping model...")
            zip_ref.extractall(tmp_dir)

    return inference({"source1.il": input_image,
                      "source1.rfieldx": rfield,
                      "source1.rfieldy": rfield,
                      "source1.placeholder": "lr_input",
                      "model.dir": tmp_unzipped,
                      "model.fullyconv": "on",
                      "output.names": f"output_{gen_fcn}",
                      "output.efieldx": efield,
                      "output.efieldy": efield,
                      "output.spcscale": ratio,
                      })
