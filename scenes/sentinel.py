"""
This module contains Sentinel-2 classes (sources and scenes).

Right now everything is ready to use THEIA L2A and L3A products.

## `Scene` based classes

The `Sentinel22AScene` and `Sentinel23AScene` classes carry metadata and sources
respectively for Sentinel-2 Level 2A and Level 3A products. They both inherit from
the generic abstract `Sentinel2SceneBase` class, which itself inherits from `Scene`.

``` mermaid
classDiagram

    Scene <|-- Sentinel2SceneBase
    Sentinel2SceneBase <|-- Sentinel22AScene
    Sentinel2SceneBase <|-- Sentinel23AScene

    class Scene{
        +datetime acquisition_date
        +int epsg
        +bbox_wgs84
        +get_metadata()
        +__repr__()
    }

    class Sentinel2SceneBase{
        +__init__(archive, tag)
        +get_file()
        +get_band()
        +get_metadata()
        +Sentinel2Source get_10m_bands()
        +Sentinel2Source get_20m_bands()

    }

    class Sentinel22AScene{
        +__init__(archive)
        +str clm_r1_msk_file
        +str clm_r2_msk_file
        +str edg_r1_msk_file
        +str edg_r2_msk_file
        +get_metadata()
    }

    class Sentinel23AScene{
        +__init__(archive)
        +str flg_r1_msk_file
        +str flg_r2_msk_file
        +get_metadata()
    }
```

### Instantiation

`Sentinel22AScene` and `Sentinel23AScene` are instantiated from the archive (.zip file)
or the product folder.
``` py
import scenes
sc_2a = scenes.sentinel.Sentinel22AScene("SENTINEL2B_..._T31TEJ_D_V1-8.zip")
sc_3a = scenes.sentinel.Sentinel23AScene("SENTINEL2X_...L3A_T31TEJ_D_V1-0.zip")
```

### Metadata

The scene metadata can be accessed with the `get_metadata()` method, like any
`scenes.core.Scene` instance.
``` py
md_dict = sc_2a.get_metadata()
for key, value in md_dict.items():
    print(f"{key}: {value}")
```

## `Source` based classes

The `Sentinel22ASource` and `Sentinel23ASource` classes carry imagery sources
respectively for Sentinel-2 Level 2A and Level 3A products. They both inherit from
the generic `Sentinel2Source` class, which itself inherits from `Source`.

``` mermaid
classDiagram

    Source <|-- Sentinel2Source
    Sentinel2Source <|-- Sentinel22ASource
    Sentinel2Source <|-- Sentinel23ASource

    class Source{
        +__init__(root_scene, out, parent=None)
        +Scene root_scene
        +Source parent
        +Source drilled(msk_vec_file, nodata=0)
        +Source cld_msk_drilled(nodata=0)
        +Source resample_over(ref_img, interpolator="nn", nodata=0)
        +Source clip_over_img(ref_img)
        +Source clip_over_vec(ref_vec)
        +Source new_source(*args)
    }

    class Sentinel2Source{
        +R1_SIZE
        +R2_SIZE
        +Sentinel2Source msk_drilled(msk_dict, exp, nodata=0)
    }

    class Sentinel22ASource{
        +Sentinel22ASource cld_msk_drilled(nodata=0)
    }

    class Sentinel23ASource{
        +Sentinel23ASource flg_msk_drilled(keep_flags_values=(3, 4), nodata=0)
    }
```

The sources carry the following images for Level 2 and Level 3 products:

- The 10m spacing channels, in the following order: 4, 3, 2, 8
- The 20m spacing channels, in the following order: 5, 6, 7, 8a, 11, 12

```
bands_10m_2a = sc_2a.get_10m_bands()
bands_20m_2a = sc_2a.get_20m_bands()
bands_10m_3a = sc_3a.get_10m_bands()
bands_20m_3a = sc_3a.get_20m_bands()
```

The `Sentinel22ASource` implements the `Sentinel22ASource.cld_msk_drilled` method, that
enable to mask the cloud masks over the root source, with the specified
no-data value (default is 0).
The following example show how to derive a child source replacing the
pixels that are in the clouds with pixels at -10000 (which is the no-data
value in Theia products):
``` py
drilled = bands_10m_2a.cld_msk_drilled()
```

The `Sentinel23ASource` implements the `Sentinel23ASource.flg_msk_drilled` method, that
enable to mask the pixels on a selection of labels of the quality mask.
The following example shows how to mask pixels of anything other that land with -10000:
``` py
drilled = bands_10m_3a.flg_msk_drilled(keep_flags_values=(4,))
```

`Sentinel22ASource` and `Sentinel23ASource` inherit from `scenes.core.Source`,
hence implemented sources transformations (e.g. `scenes.core.Source.masked`,
`scenes.core.Source.clip_over_img`, `scenes.core.Source.resample_over`,
`scenes.core.Source.reproject`, etc.
``` py
clipped = drilled.clip_over_img(roi)
reprojected = clipped.reproject(epsg=4328)
```
Note that the resulting transformed `Sentinel22ASource` and `Sentinel23ASource` are still
instances of `Sentinel22ASource` and `Sentinel23ASource` even after generic operations
implemented in `scenes.core.Source`.

## Usage with pyotb

As `scenes.core.Source`, it also can be used like any `pyotb.core.otbObject`.
The following example show how to use an OTB application with a source at input.
``` py
rgb_nice = pyotb.DynamicConvert(reprojected)
rgb_nice.write("image.tif", pixel_type="uint8")
```

"""
from __future__ import annotations
import datetime
from abc import abstractmethod
from typing import Type
import pyotb
from scenes import utils
from scenes.core import Source, Scene
from scenes.raster import get_epsg_extent_bbox


class Sentinel2Source(Source):
    """Class for generic Sentinel-2 sources"""
    R1_SIZE = 10980
    R2_SIZE = 5490

    def msk_drilled(self, msk_dict: dict[int, str], exp: str, nodata: float | int = 0) -> Sentinel2Source:
        """
        Args:
            msk_dict: dict of masks
            exp: bandmath expression to form the 0-255 binary mask
            nodata: no-data value in masked output (Default value = 0)

        Returns:
            new masked source

        """
        img_size = pyotb.ReadImageInfo(self).GetParameterInt('sizex')
        bm = pyotb.BandMath({"il": msk_dict[img_size], "exp": exp})
        return self.masked(binary_mask=bm, nodata=nodata)


class Sentinel22ASource(Sentinel2Source):
    """Sentinel-2 level 2A source class"""

    def cld_msk_drilled(self, nodata: float | int = -10000) -> Sentinel22ASource:
        """Return the source drilled from the cloud mask

        Args:
            nodata: nodata value inside holes (Default value = -10000)

        Returns:
            drilled source

        """
        return self.msk_drilled(msk_dict={self.R1_SIZE: self.root_scene.cld_r1_msk_file,
                                          self.R2_SIZE: self.root_scene.cld_r2_msk_file},
                                exp="im1b1==0?255:0",
                                nodata=nodata)


class Sentinel23ASource(Sentinel2Source):
    """Sentinel-2 level 3A source class"""

    def flg_msk_drilled(self, keep_flags_values: tuple[int] = (3, 4),
                        nodata: float | int = -10000) -> Sentinel23ASource:
        """
        Return the source drilled from the FLG mask

        Args:
            keep_flags_values: flags values to keep (Default value = (3, 4)). Can be:

               - 0 = No data
               - 1 = Cloud
               - 2 = Snow
               - 3 = Water
               - 4 = Land
               (source: https://labo.obs-mip.fr/multitemp/theias-l3a-product-format/)
            nodata: nodata value inside holes (Default value = -10000)

        Returns:
            drilled source

        """
        exp = "||".join([f"im1b1=={val}" for val in keep_flags_values]) + "?255:0"
        return self.msk_drilled(msk_dict={self.R1_SIZE: self.root_scene.flg_r1_msk_file,
                                          self.R2_SIZE: self.root_scene.flg_r2_msk_file},
                                exp=exp,
                                nodata=nodata)


class Sentinel2SceneBase(Scene):
    """Base class for Sentinel-2 images"""

    @abstractmethod
    def __init__(self, archive: str, tag: str, source_class: Type[Source]):
        """
        Args:
            archive: product .zip or directory
            tag: pattern to match in filenames, e.g. "FRE"
        """
        self.archive = archive

        # Retrieve the list of .tif files
        is_zip = self.archive.lower().endswith(".zip")
        if is_zip:
            print("Input type is a .zip archive")
            files = utils.list_files_in_zip(self.archive)
            self.files = [utils.to_vsizip(self.archive, f) for f in files]
        else:
            print("Input type is a directory")
            self.files = utils.find_files_in_all_subdirs(self.archive, "*.tif", case_sensitive=False)

        # Assign bands files
        self.band2_file = self.get_band(tag, "B2")
        self.band3_file = self.get_band(tag, "B3")
        self.band4_file = self.get_band(tag, "B4")
        self.band8_file = self.get_band(tag, "B8")
        self.band5_file = self.get_band(tag, "B5")
        self.band6_file = self.get_band(tag, "B6")
        self.band7_file = self.get_band(tag, "B7")
        self.band8a_file = self.get_band(tag, "B8A")
        self.band11_file = self.get_band(tag, "B11")
        self.band12_file = self.get_band(tag, "B12")

        # EPSG, bbox
        epsg, _, bbox_wgs84 = get_epsg_extent_bbox(self.band2_file)

        # Date
        onefile = utils.basename(self.band2_file)  # SENTINEL2A_20180630-105440-000_L2A_T31TEJ_D_V1-8
        datestr = onefile.split("_")[1]  # 20180630-105440
        acquisition_date = datetime.datetime.strptime(datestr, '%Y%m%d-%H%M%S-%f')

        # Source class
        self.source_class = source_class

        # Call parent constructor
        super().__init__(acquisition_date=acquisition_date, bbox_wgs84=bbox_wgs84, epsg=epsg)

    def get_file(self, endswith: str) -> str:
        """
        Return the specified file.

        Args:
            endswith: filtered extension

        Returns:
            the file

        Raises:
            ValueError: If none or multiple candidates are found.

        """
        filtered_files_list = [f for f in self.files if f.endswith(endswith)]
        nb_matches = len(filtered_files_list)
        if nb_matches != 1:
            raise ValueError(f"Cannot instantiate Sentinel-2 Theia product from {self.archive}:"
                             f"{nb_matches} occurrence(s) of file with suffix \"{endswith}\"")
        return filtered_files_list[0]

    def get_band(self, suffix1: str, suffix2: str) -> str:
        """
        Return the file path for the specified band.

        Args:
            suffix1: suffix 1, e.g. "FRE"
            suffix2: suffix 2, e.g. "B3"

        Returns:
            file path for the band

        Raises:
            ValueError: If none or multiple candidates are found.

        """
        return self.get_file(endswith=f"_{suffix1}_{suffix2}.tif")

    def get_10m_bands(self) -> Sentinel2Source:
        """
        Returns 10m spacing bands

        Returns:
            10m spectral bands (Red, Green, Blue, Near-infrared)

        """
        concatenate_10m_bands = pyotb.ConcatenateImages([self.band4_file,
                                                         self.band3_file,
                                                         self.band2_file,
                                                         self.band8_file])
        return self.source_class(self, concatenate_10m_bands)

    def get_20m_bands(self) -> Sentinel2Source:
        """
        Returns 20m spacing bands

        Returns:
            20m spectral bands (B6, B7, B8a, B11, B12)

        """
        concatenate_20m_bands = pyotb.ConcatenateImages([self.band5_file,
                                                         self.band6_file,
                                                         self.band7_file,
                                                         self.band8a_file,
                                                         self.band11_file,
                                                         self.band12_file])
        return self.source_class(self, concatenate_20m_bands)

    def get_metadata(self) -> dict[str, any]:
        """
        Get metadata

        Returns:
            metadata

        """
        metadata = super().get_metadata()
        metadata.update({
            "Archive": self.archive,
            "Band 2": self.band2_file,
            "Band 3": self.band3_file,
            "Band 4": self.band4_file,
            "Band 8": self.band8_file,
            "Band 5": self.band5_file,
            "Band 6": self.band6_file,
            "Band 7": self.band7_file,
            "Band 8a": self.band8a_file,
            "Band 11": self.band11_file,
            "Band 12": self.band12_file,
        })
        return metadata


class Sentinel22AScene(Sentinel2SceneBase):
    """
    Sentinel-2 level 2A scene class.

    The class carries all the metadata from the root_scene, and can be used to retrieve its sources.

    """

    def __init__(self, archive: str):
        """
        Args:
            archive: .zip file or folder. Must be a product from MAJA.
        """
        # Call parent constructor
        super().__init__(archive=archive, tag="FRE", source_class=Sentinel22ASource)

        # Additional rasters
        self.clm_r1_msk_file = self.get_file("_CLM_R1.tif")
        self.edg_r1_msk_file = self.get_file("_EDG_R1.tif")
        self.clm_r2_msk_file = self.get_file("_CLM_R2.tif")
        self.edg_r2_msk_file = self.get_file("_EDG_R2.tif")

    def get_metadata(self) -> dict[any]:
        """
        Get metadata

        Returns:
            metadata

        """
        metadata = super().get_metadata()
        metadata.update({
            "CLD R1": self.clm_r1_msk_file,
            "EDG R1": self.edg_r1_msk_file,
            "CLD R2": self.clm_r2_msk_file,
            "EDG R2": self.edg_r2_msk_file,
        })
        return metadata


class Sentinel23AScene(Sentinel2SceneBase):
    """Sentinel-2 level 3A scene class.

    The class carries all the metadata from the root_scene, and can be used to retrieve its sources.

    """

    def __init__(self, archive: str):
        """
        Args:
            archive: .zip file or folder. Must be a product from WASP.
        """
        super().__init__(archive=archive, tag="FRC", source_class=Sentinel23ASource)

        # Additional rasters
        self.flg_r1_msk_file = self.get_file("_FLG_R1.tif")
        self.flg_r2_msk_file = self.get_file("_FLG_R2.tif")

    def get_metadata(self) -> dict[str, any]:
        """
        Get metadata

        Returns:
            metadata

        """
        metadata = super().get_metadata()
        metadata.update({
            "FLG R1": self.flg_r1_msk_file,
            "FLG R2": self.flg_r2_msk_file,
        })
        return metadata


def get_scene(archive: str) -> Sentinel22AScene | Sentinel23AScene:
    """
    Return the right sentinel scene instance from the given archive (L2A or L3A)

    Args:
        archive: L3A or L3A archive

    Returns:
        a Sentinel23AScene or Sentinel22AScene instance

    """
    splits = utils.basename(archive).split("_")
    if len(splits) == 5:
        level = splits[2]
        if level == "L3A":
            return Sentinel23AScene(archive)
        if level == "L2A":
            return Sentinel22AScene(archive)
    print(f"Warning: file {archive} is not a valid Sentinel-2 product")
    return None


def get_local_scenes(root_dir: str, tile: str = None) -> list[Sentinel22AScene | Sentinel23AScene]:
    """
    Retrieve the sentinel scenes in the directory

    Args:
        root_dir: directory
        tile: tile name (optional) e.g. 31TEJ

    Returns:
        a list of sentinel scenes instances

    """
    scenes_list = []
    archives = utils.find_files_in_all_subdirs(pth=root_dir, pattern="*.zip", case_sensitive=False)
    for archive in archives:
        candidate = get_scene(archive)
        if candidate:
            tile_name = archive.split("_")[3]
            if not tile or tile_name == tile:
                scenes_list.append(candidate)
    return scenes_list


def get_downloaded_scenes(download_results: dict[str, dict[str, str]]) -> list[Sentinel22AScene | Sentinel23AScene]:
    """
    Retrieve the sentinel scenes from the download results from the TheiaDownloader

    Args:
        download_results: dict as generated from the TheiaDownloader

    Returns:
        a list of sentinel scenes instances

    """
    scenes_list = []
    for _, products in download_results.items():
        for _, dic in products.items():
            archive = dic['local_file']
            scenes_list.append(get_scene(archive))
    return scenes_list
