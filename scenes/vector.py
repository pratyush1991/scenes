"""
This module contains a set of functions to deal with OGR geometries
"""
from __future__ import annotations
from osgeo import osr, ogr
from scenes.spatial import reproject_coords, epsg2srs, BoundingBox


def ogr_open(vector_file: str) -> ogr.DataSource:
    """
    Return the vector dataset from a vector file. If the vector is empty, None is returned.

    Args:
        vector_file: input vector file

    Returns:
        ogr ds, or None (if error)

    """
    poly_ds = ogr.Open(vector_file)
    if poly_ds is None:
        raise ValueError(f"ERROR: vector layer {vector_file} is None!")
    return poly_ds


def get_bbox_wgs84(vector_file: str) -> BoundingBox:
    """
    Returns the bounding box in WGS84 CRS from a vector data

    Args:
        vector_file: vector data filename

    Returns:
        bounding box in WGS84 CRS (BoundingBox instance)

    """
    poly_ds = ogr_open(vector_file=vector_file)
    poly_layer = poly_ds.GetLayer()
    extent = poly_layer.GetExtent()
    coords = [(extent[0], extent[2]), (extent[1], extent[3])]
    src_srs = poly_layer.GetSpatialRef()
    tgt_srs = epsg2srs(4326)
    [(xmin, ymin), (xmax, ymax)] = reproject_coords(coords=coords,  # pylint: disable=unbalanced-tuple-unpacking
                                                    src_srs=src_srs,
                                                    tgt_srs=tgt_srs)
    return BoundingBox(xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)


def poly_union(layer: ogr.Layer) -> ogr.Geometry:
    """
    Compute the union all the geometrical features of layer.

    Args:
        layer: The layer

    Returns:
        the union of the layer's polygons (as a geometry)

    """
    union1 = None
    for feat in layer:
        geom = feat.GetGeometryRef()
        if union1 is None:
            union1 = geom.Clone()
        else:
            union1 = union1.Union(geom)

    return union1


def reproject_ogr_layer(layer: ogr.Layer, epsg: int = 4326) -> ogr.Geometry:
    """
    Reproject the input polygon

    Args:
        layer: OGR layer
        epsg: EPSG number (int)

    Returns:
        New polygon projected in the specified CRS

    """

    # set spatial reference and transformation
    sourceprj = layer.GetSpatialRef()
    targetprj = epsg2srs(epsg)
    transform = osr.CoordinateTransformation(sourceprj, targetprj)

    # apply transformation
    poly = poly_union(layer)
    poly.Transform(transform)
    return poly
