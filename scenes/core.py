"""
This module provides the core classes of the library, which aims to handle the access to the rasters delivered by
different EO products (Spot-6/7, Sentinel-2, ...)
"""
from __future__ import annotations
import pickle
from abc import ABC
import datetime
import pyotb

from scenes.spatial import BoundingBox
from scenes.vector import ogr_open


def save_scenes(scenes_list: list[Scene], pickle_file: str):
    """
    Use pickle to save scenes

    Args:
        scenes_list: a list of Scene instances
        pickle_file: pickle file

    """
    with open(pickle_file, "wb") as w:
        pickle.dump(scenes_list, w)


def load_scenes(pickle_file: str) -> list[Scene]:
    """
    Use pickle to load scenes

    Args:
        pickle_file: pickle file

    Returns:
        list of Scene instances

    """
    with open(pickle_file, "rb") as r:
        return pickle.load(r)


class Source(pyotb.Output):
    """
    Source class.

    Holds common operations on image sources (e.g. drill, resample, extract an ROI, etc.)
    Inherits from pyotb.Output

    """

    def __init__(self, root_scene: Scene, out: str | pyotb.core.otbObject | Source, parent: Source = None,
                 output_parameter_key: str = 'out'):
        """
        Args:
            root_scene: root Scene instance
            out: image to deliver (can be an image filename (str), a pyotb.App, etc.)
            parent: parent Source instance
            output_parameter_key: output parameter key of the app of `out`

        """
        assert isinstance(root_scene, Scene), f"root_scene type is {type(root_scene)}"
        self.root_scene = root_scene  # root scene
        # Here we call the pyotb.Output() constructor.
        # Since it can only be called with pyotb apps, we do the following:
        # - if the output is a str, (e.g. the original dimap filename), we instantiate a pyotb.Input(),
        # - else we use the original output (should be pyotb application)
        app = out  # Fine for all otbApplication, pyotb.App based classes
        if isinstance(out, str):
            app = pyotb.Input(out)
        elif isinstance(out, pyotb.Output):
            app = out.pyotb_app
        super().__init__(app=app, output_parameter_key=output_parameter_key)
        assert parent is not self, "You cannot assign a new source to its parent instance"
        self.parent = parent  # parent source (is another Source instance)
        self._app_stack = []  # list of otb applications or output to keep trace

    def new_source(self, *args, **kwargs) -> Source:
        """
        Return a new Source instance with new apps added at the end of the pipeline.

        Args:
            *args: list of pyotb.app instances to append to the existing pipeline
            **kwargs: some keyword arguments for Source instantiation

        Returns:
            new source

        """
        for new_app in args:
            self._app_stack.append(new_app)
        return self.__class__(root_scene=self.root_scene, out=self._app_stack[-1], parent=self, **kwargs)

    def drilled(self, msk_vec_file: str, inside: bool = True, nodata: float | int = 0) -> Source:
        """
        Return the source drilled from the input vector data.

        The default behavior is that the hole is made inside the polygon.
        This can be changed setting the "inside" parameter to False.

        Args:
            msk_vec_file: input vector data filename
            inside: whether the drill is happening inside the polygon or outside (Default value = True)
            nodata: nodata value inside holes (Default value = 0)

        Returns:
            drilled source

        """
        if ogr_open(msk_vec_file):
            # Vector data not empty
            rasterization = pyotb.Rasterization({"in": msk_vec_file,
                                                 "im": self,
                                                 "mode": "binary",
                                                 "mode.binary.foreground": 0 if inside else 255,
                                                 "background": 255 if inside else 0})
            return self.masked(binary_mask=rasterization, nodata=nodata)
        return self  # Nothing but a soft copy of the source

    def masked(self, binary_mask: str | pyotb.core.otbObject, nodata: float | int = 0) -> Source:
        """
        Return the source masked from an uint8 binary raster (0 or 1..255).

        Pixels are set to "nodata" where the mask values are 0.

        Args:
            binary_mask: input mono-band binary raster filename
            nodata: nodata value for rejected values (Default value = 0)

        Returns:
            masked source

        """
        manage_nodata = pyotb.ManageNoData({"in": self,
                                            "mode": "apply",
                                            "mode.apply.mask": binary_mask,
                                            "mode.apply.ndval": nodata})
        return self.new_source(binary_mask, manage_nodata)

    def resample_over(self, ref_img: str | pyotb.core.otbObject, interpolator: str = "bco",
                      nodata: float | int = 0) -> Source:
        """
        Return the source superimposed over the input image

        Args:
            ref_img: reference image
            interpolator: interpolator (Default value = "bco")
            nodata: no data value (Default value = 0)

        Returns:
            resampled image source

        """
        superimpose = pyotb.Superimpose({"inm": self,
                                         "inr": ref_img,
                                         "interpolator": interpolator,
                                         "fv": nodata})
        return self.new_source(ref_img, superimpose)

    def clip_over_img(self, ref_img: str | pyotb.core.otbObject) -> Source:
        """
        Return the source clipped over the ROI specified by the input image extent

        Args:
            ref_img: reference image

        Returns:
            ROI clipped source

        """
        extract_roi = pyotb.ExtractROI({"in": self,
                                        "mode": "fit",
                                        "mode.fit.im": ref_img})
        return self.new_source(ref_img, extract_roi)

    def clip_over_vec(self, ref_vec: str) -> Source:
        """
        Return the source clipped over the ROI specified by the input vector extent

        Args:
            ref_vec: reference vector data

        Returns:
            ROI clipped source

        """
        return self.new_source(pyotb.ExtractROI({"in": self,
                                                 "mode": "fit",
                                                 "mode.fit.vect": ref_vec}))

    def reproject(self, epsg: int, interpolator: str = "bco") -> Source:
        """
        Reproject the source into the specified EPSG

        Args:
            epsg: EPSG (int)
            interpolator: interpolator (Default value = "bco")

        Returns:
            reprojected source

        """
        if self.root_scene.epsg != epsg:
            return self.new_source(pyotb.OrthoRectification({"io.in": self,
                                                             "map": "epsg",
                                                             "map.epsg.code": epsg,
                                                             "interpolator": interpolator}),
                                   output_parameter_key='io.out')
        return self  # Nothing but a soft copy of the source


class Scene(ABC):
    """
    Scene class.

    The class carries all the metadata from the scene, and can be used to retrieve its imagery.

    """

    def __init__(self, acquisition_date: datetime.datetime, bbox_wgs84: BoundingBox, epsg: int):
        """
        Constructor

        Args:
            acquisition_date: Acquisition date
            bbox_wgs84: Bounding box, in WGS84 coordinates reference system
            epsg: EPSG code

        """
        assert isinstance(acquisition_date, datetime.datetime), "acquisition_date must be a datetime.datetime instance"
        self.acquisition_date = acquisition_date
        self.bbox_wgs84 = bbox_wgs84
        assert isinstance(epsg, int), "epsg must be an int"
        self.epsg = epsg

    def get_metadata(self) -> dict[str, any]:
        """
        Metadata accessor

        Returns:
            metadata

        """
        return {
            "Acquisition date": self.acquisition_date,
            "Bounding box (WGS84)": self.bbox_wgs84,
            "EPSG": self.epsg,
        }

    def get_serializable_metadata(self) -> dict[str, str]:
        """
        Enable one instance to be used with print() to show the metadata items

        Returns:
            metadata items as str
        """
        return {k: str(v) for k, v in self.get_metadata().items()}

    def __repr__(self) -> str:
        """
        Enable one instance to be used with print()

        Returns:
            a string summarizing the metadata

        """
        return "\n".join([f"{key}: {value}" for key, value in self.get_metadata().items()])
