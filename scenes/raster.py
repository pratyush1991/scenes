"""
This module contains a set of functions to deal with GDAL datasets (rasters)
"""
from __future__ import annotations
import pyotb
from osgeo import osr, gdal
from scenes.vector import reproject_coords
from scenes.spatial import epsg2srs, coord_list_to_bbox, BoundingBox


def get_epsg(gdal_ds: gdal.Dataset) -> int:
    """
    Get the EPSG code of a GDAL dataset

    Args:
        gdal_ds: GDAL dataset

    Returns:
        EPSG code (int)

    """
    proj = osr.SpatialReference(wkt=gdal_ds.GetProjection())
    epsg = proj.GetAttrValue('AUTHORITY', 1)
    assert str(epsg).isdigit()
    return int(epsg)


def get_extent(raster: gdal.Dataset | str | pyotb.core.otbObject) -> tuple[tuple]:
    """
    Return list of corners coordinates from a raster

    Args:
        raster: GDAL dataset, str (raster filename) or otbobject

    Returns:
        list of coordinates

    """
    if isinstance(raster, pyotb.core.otbObject):
        info = pyotb.ReadImageInfo(raster)
        spcx = info.GetParameterFloat('spacingx')
        spcy = info.GetParameterFloat('spacingy')
        orix = info.GetParameterFloat('originx')
        oriy = info.GetParameterFloat('originy')
        szx = info.GetParameterFloat('sizex')
        szy = info.GetParameterFloat('sizey')
        xmin = orix - .5 * spcx
        ymax = oriy - .5 * spcy
    else:
        gdal_ds = gdal.Open(raster) if isinstance(raster, str) else raster
        xmin, spcx, _, ymax, _, spcy = gdal_ds.GetGeoTransform()
        szx, szy = gdal_ds.RasterXSize, gdal_ds.RasterYSize
    xmax = xmin + szx * spcx
    ymin = ymax + szy * spcy

    return (xmin, ymax), (xmax, ymax), (xmax, ymin), (xmin, ymin)


def get_projection(raster: gdal.Dataset | str | pyotb.core.otbObject) -> str:
    """
    Returns the projection (as str) of a raster.

    Args:
        raster: GDAL dataset, str (raster filename) or otbobject

    Returns:
        a str

    """
    if isinstance(raster, pyotb.core.otbObject):
        return pyotb.ReadImageInfo(raster).GetParameterString("projectionref")
    gdal_ds = gdal.Open(raster) if isinstance(raster, str) else raster
    return gdal_ds.GetProjection()


def get_extent_wgs84(raster: gdal.Dataset | str | pyotb.core.otbObject) -> tuple[tuple]:
    """
    Returns the extent in WGS84 CRS from a raster

    Args:
        raster: GDAL dataset, str (raster filename) or otbobject

    Returns:
        extent: coordinates in WGS84 CRS

    """
    coords = get_extent(raster)
    src_srs = osr.SpatialReference()
    projection = get_projection(raster)
    src_srs.ImportFromWkt(projection)
    tgt_srs = epsg2srs(4326)

    return reproject_coords(coords, src_srs, tgt_srs)


def get_epsg_extent_bbox(filename: str) -> tuple[int, tuple, BoundingBox]:
    """
    Returns (epsg, extent_wgs84) from a raster file that GDAL can open.

    Args:
        filename: file name

    Returns:
        (epsg, extent_wgs84, bbox_wgs84)

    """
    gdal_ds = gdal.Open(filename)
    epsg = get_epsg(gdal_ds)
    extent_wgs84 = get_extent_wgs84(gdal_ds)
    bbox_wgs84 = coord_list_to_bbox(extent_wgs84)

    return epsg, extent_wgs84, bbox_wgs84


def get_bbox_wgs84(raster: gdal.Dataset | str | pyotb.core.otbObject) -> BoundingBox:
    """
    Returns the bounding box from a raster.

    Args:
        raster: raster filename (str) or GDAL dataset

    Returns:
        a BoundingBox instance

    """
    extent = get_extent_wgs84(raster)
    return coord_list_to_bbox(extent)
