# -*- coding: utf-8 -*-
"""
This module aims to deal with dates.

---

The `datetime.datetime` class is used as internal date type.
``` py
dt = datetime.datetime(year=2020, month=12, day=2)
```
Is equivalent to:
``` py
dt = str2datetime("02-12-2020")
dt = str2datetime("2020-12-02")
dt = str2datetime("02/12/2020")
```

The `any2datetime` method returns a `datetime.datetime` instance, whatever the input is (`str` or
`datetime.datetime`).
``` py
dt1 = datetime.datetime(year=2020, month=12, day=2)
dt2 = str2datetime("02-12-2020")
dt1_2 = any2datetime(dt1)  # same
dt2_2 = any2datetime("02-12-2020")  # same
```

The `get_timestamp` method converts a `datetime.datetime` instance into a number of seconds (int).
``` py
ts = get_timestamp(dt)  # 1606780800.0
```
"""
from __future__ import annotations
import datetime


MINDATE = datetime.datetime.strptime("1000-01-01", "%Y-%m-%d")
MAXDATE = datetime.datetime.strptime("3000-01-01", "%Y-%m-%d")


def get_timestamp(dt: datetime.datetime) -> int:
    """
    Converts datetime.datetime into a timestamp (in seconds)

    Args:
        dt: datetime.datetime instance

    Returns:
        timestamp (in seconds)

    """
    return dt.replace(tzinfo=datetime.timezone.utc).timestamp()


def str2datetime(datestr: str) -> datetime.datetime:
    """
    Converts an input date as string into a datetime instance.

    Args:
        datestr: date (str) in the format "YYYY-MM-DD" or "DD/MM/YYYY" or "DD-MM-YYYY"

    Returns:
        A datetime.datetime instance

    """
    # source (with a few enhancements):
    # https://stackoverflow.com/questions/23581128/how-to-format-date-string-via-multiple-formats-in-python
    assert isinstance(datestr, str), "Input must be a str!"
    formats = ('%Y-%m-%d', '%d/%m/%Y', '%d-%m-%Y')
    for fmt in formats:
        try:
            return datetime.datetime.strptime(datestr, fmt)
        except ValueError:
            pass
    raise ValueError(f'No valid date format found. Accepted formats are {formats}. Input was: {datestr}')


def any2datetime(str_or_datetime: str | datetime.datetime) -> datetime.datetime:
    """
    Normalizes the input such as the returned object is a datetime.datetime instance.

    Args:
        str_or_datetime: a str (see `str2datetime()` for supported dates formats) or a datetime.datetime

    Returns:
        A datetime.datetime instance

    """
    if isinstance(str_or_datetime, datetime.datetime):
        return str_or_datetime
    assert isinstance(str_or_datetime, str), "Date must be a str, or a datetime.datetime instance!"
    return str2datetime(str_or_datetime)
